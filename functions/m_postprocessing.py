"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    result = np.sqrt(x**2 + y**2 + z**2)
    
    return result
    


def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    # Ensure that time and data have the same length
    if len(time) != len(data):
        raise ValueError("Input arrays 'time' and 'data' must have the same length.")

    # Create interpolation points linearly distributed between min(time) and max(time)
    interpolation_points = np.linspace(min(time), max(time), len(time))

    # Perform linear Newtonian interpolation
    interpolated_values = np.interp(interpolation_points, time, data)

    return interpolation_points, interpolated_values

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    # Ensure the mean of the data is zero
    x_mean = np.mean(x)
    x -= x_mean
    
    # Calculate the FFT of x
    fft_result = np.fft.fft(x)
    
    # Calculate the corresponding frequencies
    frequencies = np.fft.fftfreq(len(time), time[1] - time[0])
    
    # Return amplitude and frequency for positive frequencies only
    positive_frequencies_mask = frequencies >= 0
    
    amplitude = np.abs(fft_result[positive_frequencies_mask])
    frequency = frequencies[positive_frequencies_mask]

    
    return amplitude, frequency