import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_lavalrotor_2.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Extracting UUID from sensor_settings_dict
accelerometer_uuid = sensor_settings_dict['ID']

# Initialize data_structure
data_structure = {
    accelerometer_uuid: {
        'acceleration_x': [],
        'acceleration_y': [],
        'acceleration_z': [],
        'timestamp': [],
    }
}

# Display the initialized data_structure
print("Initialized Data Structure:")
print(json.dumps(data_structure, indent=2, default=str))
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
measure_duration_in_s = 20


start_timestamp = int(time.time())


end_timestamp = start_timestamp + int(measure_duration_in_s)

while int(time.time() ) < end_timestamp:
    timestamp = int(time.time() ) - start_timestamp

 
    acceleration_x, acceleration_y, acceleration_z = accelerometer.acceleration
    data_structure['1eeaf353-55b5-6e82-b2eb-c521c869dead']['acceleration_x'].append(acceleration_x)
    data_structure['1eeaf353-55b5-6e82-b2eb-c521c869dead']['acceleration_y'].append(acceleration_y)
    data_structure['1eeaf353-55b5-6e82-b2eb-c521c869dead']['acceleration_z'].append(acceleration_z)
    data_structure['1eeaf353-55b5-6e82-b2eb-c521c869dead']['timestamp'].append(timestamp)

    time.sleep(0.001)

print("Messung abgeschlossen.")
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


output_file_path = os.path.join(path_measurement_folder, f"{os.path.basename(path_measurement_folder)}.h5")

units_attributes = {
    'acceleration_x': 'm/s^2',
    'acceleration_y': 'm/s^2',
    'acceleration_z': 'm/s^2',
    'timestamp': 's'
}


with h5py.File(output_file_path, 'w') as file:
    
    group = file.create_group('1eeaf353-55b5-6e82-b2eb-c521c869dead')


    for key, value in data_structure['1eeaf353-55b5-6e82-b2eb-c521c869dead'].items():
        dataset = group.create_dataset(key, data=value)


        unit_attribute = units_attributes.get(key)
        if unit_attribute:
            dataset.attrs['unit'] = unit_attribute

print(f"Die Messdaten wurden erfolgreich in {output_file_path} gespeichert.")


# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
